﻿using System;
using System.Collections.Generic;

namespace BinarySearch
{
    /// <summary>
    /// Represents class for binary searching in arrays.
    /// </summary>
    public static class BinarySearch
    {
        /// <summary>
        /// Implements binary search using default comparer.
        /// </summary>
        /// <param name="source">Source array.</param>
        /// <param name="value">Value to find.</param>
        /// <returns>
        /// The position of an element with a given value in sorted array.
        /// If element is not found returns null.
        /// </returns>
        /// <exception cref="ArgumentNullException">Thrown when source array is null.</exception>
        /// <exception cref="ArgumentException">Thrown when T does not implement IComparable interface.</exception>
        public static int? SearchBinary<T>(this T[] source, T value)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            return Search(source, value, Comparer<T>.Default);
        }
        
        /// <summary>
        /// Implements binary search using specified comparer.
        /// </summary>
        /// <param name="source">Source sorted array.</param>
        /// <param name="value">Value to search.</param>
        /// <param name="comparer">Comparer.</param>
        /// <returns>
        /// The position of an element with a given value in sorted array.
        /// If element is not found returns null.
        /// </returns>
        /// <exception cref="ArgumentNullException">Thrown when array is null.</exception>
        /// <exception cref="ArgumentException">Thrown when T does not implement IComparable interface.</exception>
        public static int? SearchBinary<T>(this T[] source, T value, IComparer<T> comparer)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            comparer ??= Comparer<T>.Default;

            return Search(source, value, comparer);
        }

        private static int? Search<T>(this T[] source, T value, IComparer<T> comparer)
        {
            int leftBorder = 0;
            int rightBorder = source.Length - 1;

            while (leftBorder <= rightBorder)
            {
                int index = (rightBorder + leftBorder) / 2;

                switch (comparer.Compare(source[index], value))
                {
                    case > 0:
                        rightBorder = index - 1;
                        break;
                    case < 0:
                        leftBorder = index + 1;
                        break;
                    default:
                        return index;
                }
            }

            return null;
        }
    }
}