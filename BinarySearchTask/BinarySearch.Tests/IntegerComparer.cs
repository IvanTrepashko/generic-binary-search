﻿using System.Collections.Generic;

namespace BinarySearch.Tests
{
    public class IntegerComparer : Comparer<int>
    {
        public override int Compare(int x, int y)
        {
            return x.CompareTo(y);
        }
    }
}