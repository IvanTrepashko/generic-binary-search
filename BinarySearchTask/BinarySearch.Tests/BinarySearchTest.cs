using System;
using System.Collections.Generic;
using NUnit.Framework;

#pragma warning disable CA1707

namespace BinarySearch.Tests
{
    [TestFixture]
    public class Tests
    {
        [TestCase(new[] { 6 }, 6, ExpectedResult = 0)]
        [TestCase(new[] { 1, 3, 4, 6, 8, 9, 11 }, 6, ExpectedResult = 3)]
        [TestCase(new[] { 1, 3, 4, 6, 8, 9, 11 }, 1, ExpectedResult = 0)]
        [TestCase(new[] { 1, 3, 4, 6, 8, 9, 11 }, 11, ExpectedResult = 6)]
        [TestCase(new[] { 1, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 634 }, 144, ExpectedResult = 9)]
        [TestCase(new[] { 1, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377 }, 21, ExpectedResult = 5)]
        public int BinarySearch_ReturnIndexOfValueInArray(int[] source, int value)
        {
            return source.SearchBinary(value).Value;
        }
        
        [TestCase(new[] { 6 }, 7, ExpectedResult = null)]
        [TestCase(new[] { 1, 3, 4, 6, 8, 9, 11 }, 0, ExpectedResult = null)]
        [TestCase(new[] { 1, 3, 4, 6, 8, 9, 11 }, 14, ExpectedResult = null)]
        [TestCase(new[] { 1, 3, 4, 6, 8, 9, 11 }, 11, ExpectedResult = 6)]
        [TestCase(new int[] { }, 144, ExpectedResult = null)]
        [TestCase(new[] { 1, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377 }, 21, ExpectedResult = 5)]
        public int? BinarySearch_ReturnNull(int[] source, int value)
        {
            return source.SearchBinary(value, null);
        }

        [TestCase(new []{"abc", "bcd", "cfv", "fte"},"cfv", ExpectedResult = 2)]
        public int BinarySearchInStringArray_ReturnsIndexOfValue(string[] source, string value)
        {
            return source.SearchBinary(value, Comparer<string>.Default).Value;
        }

        [Test]
        public void BinarySearchInStringArray_CustomComparer_ReturnsIndexOfValue()
        {
            string[] source = new[] {"aa", "bbb", "cccc", "ddddd", "eeeeee"};
            string value = "eeeeee";
            int expected = 4;

            int actual = source.SearchBinary<string>(value, new StringComparer()).Value;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void BinarySearchInIntegerArray_CustomComparer_ReturnsIndexOfValue()
        {
            int[] source = {1, -2, -4, 6, 7, -9, 11, -23};
            int value = 11;
            int expected = 6;

            int actual = source.SearchBinary(value, new AbsIntegerComparer()).Value;

            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void BinarySearch_CustomType_IComparableNotImplemented_ThrowsArgumentException()
        {
            Person[] source = 
            {
                new Person(),
                new Person(),
                new Person(),
                new Person()
            };

            Person value = new Person();

            Assert.Throws<ArgumentException>(() => source.SearchBinary(value));
        }
    }
}
