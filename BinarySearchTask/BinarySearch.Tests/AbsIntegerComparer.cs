﻿using System;
using System.Collections.Generic;

namespace BinarySearch.Tests
{
    public class AbsIntegerComparer : Comparer<int>
    {
        public override int Compare(int x, int y)
        {
            return Math.Abs(x).CompareTo(Math.Abs(y));
        }
    }
}