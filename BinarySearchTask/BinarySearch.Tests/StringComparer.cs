﻿using System.Collections.Generic;

namespace BinarySearch.Tests
{
    public class StringComparer : Comparer<string>
    {
        public override int Compare(string x, string y)
        {
            int xLength = x?.Length ?? 0;
            int yLength = y?.Length ?? 0;

            return xLength.CompareTo(yLength);
        }
    }
}