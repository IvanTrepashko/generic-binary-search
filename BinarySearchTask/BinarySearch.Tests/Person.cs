﻿namespace BinarySearch.Tests
{
    public class Person
    {
        public string Name { get; set; }
        
        public int Age { get; set; }
        
        public double Salary { get; set; }
    }
}